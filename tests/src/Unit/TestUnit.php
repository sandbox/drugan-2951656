<?php

namespace Drupal\Tests\devel\Unit;

//use Drupal\content_moderation\ContentPreprocess;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\node\Entity\Node;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;


class TestUnit extends UnitTestCase {
  /**
   * Test the getArgument() method.
   *
   * @covers ::getArgument
   * @dataProvider providerGetArgument
   */
  public function NOTtestUnit() {


    $user_argument = $this->getMockBuilder('Drupal\user\Plugin\views\argument_default\User')
      ->disableOriginalConstructor()
      ->setMethods(NULL)
      ->getMock();

     dpm($user_argument);
  return;

  foreach ($this->providerGetArgument() as $array) {
  list($options, $request, $expected) = $array;
  }


    $view = $this->getMockBuilder('Drupal\views\ViewExecutable')
      ->disableOriginalConstructor()
      ->setMethods(NULL)
      ->getMock();



    $view->setRequest($request);
    $display_plugin = $this->getMockBuilder('Drupal\views\Plugin\views\display\DisplayPluginBase')
      ->disableOriginalConstructor()
      ->getMock();

    //$raw = new \Drupal\views\Plugin\views\argument_default\QueryParameter([], 'query_parameter', []);
    $raw = new \Drupal\user\Plugin\views\argument_default\User([], 'query_parameter', []);
    $raw->init($view, $display_plugin, $options);
    $this->assertEquals($expected, $raw->getArgument());
  }

  /**
   * Provides data for testGetArgument().
   *
   * @return array
   *   An array of test data, with the following entries:
   *   - first entry: the options for the plugin.
   *   - second entry: the request object to test with.
   *   - third entry: the expected default argument value.
   */
  public function NOTproviderGetArgument() {
    $data = [];

    $data[] = [
      ['query_param' => 'test', 'multiple' => 'and'],
      new Request(['test' => ['data1', 'data2']]),
      'data1,data2',
    ];

    $data[] = [
      ['query_param' => 'test', 'multiple' => 'or'],
      new Request(['test' => ['data1', 'data2']]),
      'data1+data2',
    ];

    $data[] = [
      ['query_param' => 'test', 'fallback' => 'blub'],
      new Request([]),
      'blub',
    ];

    $data[] = [
      ['query_param' => 'test'],
      new Request(['test' => 'data']),
      'data',
    ];

    return $data;
  }
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

  public function testUnit() {

foreach ($this->providerGetArgument() as $array) {
list($entity_id, $entity, $options_user, $expected) = $array;
}

    $parameter_bag = $this->getMockBuilder('Symfony\Component\HttpFoundation\ParameterBag')
      ->disableOriginalConstructor()
      ->getMock();
    $parameter_bag->method('all')
      ->will($this->returnValue([$entity_id => $entity]));

    $route_match = $this->getMockBuilder('Drupal\Core\Routing\CurrentRouteMatch')
      ->disableOriginalConstructor()
      ->getMock();
    $route_match->method('getParameter')
      ->with('user')->will($this->returnValue($entity_id == 'user' ? $entity : NULL));
    $route_match->method('getParameters')
      ->will($this->returnValue($parameter_bag));

    $user_argument = $this->getMockBuilder('Drupal\user\Plugin\views\argument_default\User')
    ->setConstructorArgs([[], 'user', [], $route_match])
       ->setMethods(NULL)
       ->getMock();

    $user_argument->options['user'] = $options_user;
    $this->assertEquals($expected, $user_argument->getArgument());
dpm(             $user_argument->getArgument()                         );
  }


  /**
   * Provides data for testGetArgument().
   *
   * @return array
   *   An array of test data, with the following entries:
   *   - first entry: the id of an entity.
   *   - second entry: the entity object to test with.
   *   - third entry: the options 'user' value.
   *   - fourth entry: the expected default argument value.
   */
  public function providerGetArgument() {
    $data = [];

    $user = $this->getMock('Drupal\user\UserInterface');
    $user->expects($this->any())
      ->method('id')
      ->will($this->returnValue('1'));

    $test = $this->getMock('Drupal\user\EntityOwnerInterface');
    $test->expects($this->any())
      ->method('getOwnerId')
      ->will($this->returnValue('3'));

    $data[] = [
      'user',
      $user,
      NULL,
      '1',
    ];
    $data[] = [
      'test',
      $test,
      '1',
      '3',
    ];
    return $data;
  }

}
