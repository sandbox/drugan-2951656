<?php

namespace Drupal\devel\Controller;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\Xss;
use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\devel\DevelDumperManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\entity\BundleFieldDefinition;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Returns responses for devel module routes.
 */
class DevelController extends ControllerBase {

public $utils = <<<'STR'
$this->cache($bin = default),
$this->config($name),
$this->currentUser(),
$this->entityFormBuilder(),
$man = $this->entityManager(),
$tman = $this->entityTypeManager(),
$this->formBuilder(),
$this->getDestinationArray(),
$this->getLinkGenerator(),
$this->getLogger($channel),
$this->getRedirectDestination(),
$this->getStringTranslation(),
$this->getUrlGenerator(),
$this->keyValue($collection),
$this->l($text, Drupal\Core\Url $url),
$this->languageManager(),
$this->moduleHandler(),
$this->messenger(),
$this->stateSystemPage(),
$this->themeRegistry(),
$fm = \Drupal::service('entity_field.manager'),
STR;

public $str = <<<'STR'
lorem ipsum...
STR;

  /**
   * {@inheritdoc}
   */
  public function tmpoutPage() {tmpout();
$man = $this->entityManager();



    return [];//////////////////////////////////////////////////////////////////
  }

  /**
   * Displays the source of devel.module file.
   *
   * @return
   *   The source.
   */
  public function develDotModulePage() {
    $module_path = drupal_get_path('module', 'devel');
    $root_path = explode($module_path, dirname(__FILE__))[0];
    $module_file = $root_path . $module_path . '/devel.module';
    $edit = "<p style='font-weight: bolder;'>edit => <a href='gedit://{$module_file}'>{$module_file}</a></p>";
    $code = ccode($module_file, 0, 5000);
    $output['details'] = [
      '#type' => 'details',
      '#title' => t('Look into the devel.module file'),
    ];
    $output['details']['devel_module'] = [
      '#type' => 'markup',
      '#markup' => \Drupal\Core\Render\Markup::create($edit . $code),
    ];

    return $output;
  }

  /**
   * The dumper service.
   *
   * @var \Drupal\devel\DevelDumperManagerInterface
   */
  protected $dumper;

  /**
   * EntityDebugController constructor.
   *
   * @param \Drupal\devel\DevelDumperManagerInterface $dumper
   *   The dumper service.
   */
  public function __construct(DevelDumperManagerInterface $dumper) {
    $this->dumper = $dumper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('devel.dumper'));
  }

  /**
   * Clears all caches, then redirects to the previous page.
   */
  public function cacheClear() {
    drupal_flush_all_caches();
    $this->messenger()->addMessage($this->t('Cache cleared.'));
    return $this->redirect('<front>');
  }

  public function themeRegistry() {
    $hooks = theme_get_registry();
    ksort($hooks);
    return $this->dumper->exportAsRenderable($hooks);
  }

  /**
   * Builds the fields info overview page.
   *
   * @return array
   *   Array of page elements to render.
   */
  public function fieldInfoPage() {
    $fields = FieldStorageConfig::loadMultiple();
    ksort($fields);
    $output['fields'] = $this->dumper->exportAsRenderable($fields, $this->t('Fields'));

    $field_instances = FieldConfig::loadMultiple();
    ksort($field_instances);
    $output['instances'] = $this->dumper->exportAsRenderable($field_instances, $this->t('Instances'));

    $bundles = \Drupal::service('entity_type.bundle.info')->getAllBundleInfo();
    ksort($bundles);
    $output['bundles'] = $this->dumper->exportAsRenderable($bundles, $this->t('Bundles'));

    $field_types = \Drupal::service('plugin.manager.field.field_type')->getUiDefinitions();
    ksort($field_types);
    $output['field_types'] = $this->dumper->exportAsRenderable($field_types, $this->t('Field types'));

    $formatter_types = \Drupal::service('plugin.manager.field.formatter')->getDefinitions();
    ksort($formatter_types);
    $output['formatter_types'] = $this->dumper->exportAsRenderable($formatter_types, $this->t('Formatter types'));

    $widget_types = \Drupal::service('plugin.manager.field.widget')->getDefinitions();
    ksort($widget_types);
    $output['widget_types'] = $this->dumper->exportAsRenderable($widget_types, $this->t('Widget types'));

    return $output;
  }

  /**
   * Builds the state variable overview page.
   *
   * @return array
   *   Array of page elements to render.
   */
  public function stateSystemPage() {
    $can_edit = $this->currentUser()->hasPermission('administer site configuration');

    $header = [
      'name' => $this->t('Name'),
      'value' => $this->t('Value'),
    ];

    if ($can_edit) {
      $header['edit'] = $this->t('Operations');
    }

    $rows = [];
    // State class doesn't have getAll method so we get all states from the
    // KeyValueStorage.
    foreach ($this->keyValue('state')->getAll() as $state_name => $state) {
      $rows[$state_name] = [
        'name' => [
          'data' => $state_name,
          'class' => 'table-filter-text-source',
        ],
        'value' => [
          'data' => $this->dumper->export($state),
        ],
      ];

      if ($can_edit) {
        $operations['edit'] = [
          'title' => $this->t('Edit'),
          'url' => Url::fromRoute('devel.system_state_edit', ['state_name' => $state_name]),
        ];
        $rows[$state_name]['edit'] = [
          'data' => ['#type' => 'operations', '#links' => $operations],
        ];
      }
    }

    $output['states'] = [
      '#type' => 'devel_table_filter',
      '#filter_label' => $this->t('Search'),
      '#filter_placeholder' => $this->t('Enter state name'),
      '#filter_title' => $this->t('Enter a part of the state name to filter by.'),
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No state variables found.'),
      '#attributes' => [
        'class' => ['devel-state-list'],
      ],
    ];

    return $output;
  }

  /**
   * Builds the session overview page.
   *
   * @return array
   *   Array of page elements to render.
   */
  public function session() {
    $output['description'] = array(
      '#markup' => '<p>' . $this->t('Here are the contents of your $_SESSION variable.') . '</p>',
    );
    $output['session'] = array(
      '#type' => 'table',
      '#header' => array($this->t('Session name'), $this->t('Session ID')),
      '#rows' => array(array(session_name(), session_id())),
      '#empty' => $this->t('No session available.'),
    );
    $output['data'] = $this->dumper->exportAsRenderable($_SESSION);

    return $output;
  }

}
