<?php

namespace Drupal\devel;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class DevelDumperManager.
 */
class DevelDumperManager implements DevelDumperManagerInterface {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * The devel config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The devel dumper plugin manager.
   *
   * @var \Drupal\devel\DevelDumperPluginManagerInterface
   */
  protected $dumperManager;

  /**
   * Constructs a DevelDumperPluginManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current account.
   * @param \Drupal\devel\DevelDumperPluginManagerInterface $dumper_manager
   *   The devel dumper plugin manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountProxyInterface $account, DevelDumperPluginManagerInterface $dumper_manager) {
    $this->config = $config_factory->get('devel.settings');
    $this->account = $account;
    $this->dumperManager = $dumper_manager;
  }

  /**
   * Instances a new dumper plugin.
   *
   * @param string $plugin_id
   *   (optional) The plugin ID, defaults to NULL.
   *
   * @return \Drupal\devel\DevelDumperInterface
   *   Returns the devel dumper plugin instance.
   */
  protected function createInstance($plugin_id = NULL) {
    if (!$plugin_id || !$this->dumperManager->isPluginSupported($plugin_id)) {
      $plugin_id = $this->config->get('devel_dumper');
    }
    return $this->dumperManager->createInstance($plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function dump($input, $name = NULL, $plugin_id = NULL) {
    if (devel_user()) {
      $this->createInstance($plugin_id)->dump($input, $name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function export($input, $name = NULL, $plugin_id = NULL) {
    if (devel_user()) {
      return $this->createInstance($plugin_id)->export($input, $name);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function message($input, $name = NULL, $type = MessengerInterface::TYPE_STATUS, $plugin_id = NULL) {
    if (devel_user()) {
      $output = $this->export($input, $name, $plugin_id);
      $this->messenger()->addMessage($output, $type, TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function debug($input, $args = []) {
    static $i;
    $i = is_null($i) ? 0 : $i + 1;
    $params = [
      'name' => 'devel info fetched from a temp dir',
      'plugin_id' => NULL,
      'path' => NULL,
      'append' => FALSE,
      'repeat' => FALSE,
      'offset' => 0,
      'class_methods' => TRUE,
    ];
    extract(array_merge($params, $args));
    $name = strtoupper($name);
    if ($i < $offset || !$append && $i || ($append && (is_int($repeat) && ($i > $offset + $repeat - 1)))) {
      return;
     }
    if (!$class_methods && function_exists('kint_require')) {
      kint_require();
      \Kint::$displayClassMethods = $class_methods;
    }
    $temp = file_directory_temp();
    $tmp = (array) explode('/', $temp);
    $tmp = $tmp[0] == 'tmp' || (isset($tmp[1]) && $tmp[1] == 'tmp') ? $temp : '/tmp';
    $dir = is_string($path) ? dirname($path) : $tmp .'/drupal_debug_files';
    $file = file_prepare_directory($dir, FILE_CREATE_DIRECTORY) || $path ? (is_string($path) ? $path : tempnam($dir, 'file_')) : FALSE;
    if (!$file) {
      $tmp = \Drupal::root() . '/tmp';
      !is_dir($tmp) && mkdir($tmp, 0777, TRUE);
      !is_dir($tmp . '/drupal_debug_files') && mkdir($tmp . '/drupal_debug_files');
      $file = tempnam($tmp . '/drupal_debug_files', 'file_');
    }
    $invalid = !$file ? 'Devel was unable to write to '. $dir . '.' : FALSE;
    $denied = !devel_user() ? 'The access to Devel information is denied.' : FALSE;
    $plugin_id = $plugin_id && $this->dumperManager->isPluginSupported($plugin_id) ? $plugin_id : ($this->dumperManager->isPluginSupported('kint') ? 'kint' : 'default');
    $string = !$invalid && !$denied ? $this->export($input, "{$name}: #{$i}", $plugin_id) : implode(' ', [(string) $invalid, (string) $denied]);
    $failed = file_put_contents($file, $string) === FALSE ? : FALSE;
    $unknown = $failed && file_put_contents($tmp . '/dd_failed', 'Contents put into ' . $file . ' is failed.');

    if ($append) {
      $success = file_put_contents($tmp . '/dd_files_list', "{$file}|", FILE_APPEND);
    }
    else {
      $success = file_put_contents($tmp . '/dd_files_list', $file);
    }

    return $failed ? $unknown : $success;
   }

  /**
   * {@inheritdoc}
   */
  public function dumpOrExport($input, $name = NULL, $export = TRUE, $plugin_id = NULL) {
    if (devel_user()) {
      $dumper = $this->createInstance($plugin_id);
      if ($export) {
        return $dumper->export($input, $name);
      }
      $dumper->dump($input, $name);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function exportAsRenderable($input, $name = NULL, $plugin_id = NULL) {
    if (devel_user()) {
      return $this->createInstance($plugin_id)->exportAsRenderable($input, $name);
    }
    return [];
  }

  /**
   * Checks whether a user has access to devel information.
   *
   * @return bool
   *   TRUE if the user has the permission, FALSE otherwise.
   */
  protected function hasAccessToDevelInformation() {
    return $this->account && $this->account->hasPermission('access devel information');
  }

}
